package Observer;

import java.util.HashSet;
import java.util.Set;

public class Observable {
	private Set<Observer> observers;
	
	public Observable() {
		super();
		observers = new HashSet<Observer>();
	}

	public void addObserver(Observer observer) {
		observers.add(observer);
	}

	public void removeObserver(Observer observer) {
		observers.remove(observer);
	}

	public void notifyObservers() {
		for (Observer observer : observers)
			observer.update();
	}
}
